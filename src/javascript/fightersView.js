import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import Fighter from "./fighter";

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();
  fighters = new Array();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  handleFighterClick(event, fighter) {
    if (!this.fightersDetailsMap.has(fighter._id)) {
      fighterService.getFighterDetails(fighter._id)
        .then(fighterDetailsId => {
          fighterDetailsId.health = prompt('Input health (default ' + `${fighterDetailsId.health}` + ')', `${fighterDetailsId.health}`);
          fighterDetailsId.attack = prompt('Input attack (default ' + `${fighterDetailsId.attack}` + ')', `${fighterDetailsId.attack}`);
          fighterDetailsId.defense = prompt('Input health (default ' + `${fighterDetailsId.defense}` + ')', `${fighterDetailsId.defense}`);

          fighter.details = fighterDetailsId;
          this.fighters.push(fighter);

          if (this.fighters.length === 2) {
            if (this.fighters[0].name === this.fighters[1].name) {
              this.fighters.length = 0;
              alert('A fight with a shadow.. Hmm');
            } else {
              const result = confirm("Fighters are ready! Let's start the fight?");
              if (result) {
                fight(...this.fighters);
              }
            }

            this.fighters.length = 0;
          }
        });
    }
  }
}

function fight(firstFighter, secondFighter) {
  let turn = 0;

  const interval = setInterval(() => {
    if (turn === 0) {
      let damage = +Math.abs(new Fighter().returnDamage(firstFighter));
      secondFighter.details.health = secondFighter.details.health - damage;

      if (damage === 0) {
        alert(firstFighter.details.name + ' missed.. Aww');
      } else {
        alert(secondFighter.details.name + ' hitted: -' + damage + '. Current health of ' + secondFighter.details.name + ': ' + secondFighter.details.health);
      }

      if (secondFighter.details.health <= 0) {
        clearInterval(interval);
        alert('Fight end. ' + 'Winner: ' + firstFighter.details.name);
      }

      turn++;
    } else {
      let damage = +Math.abs(new Fighter().returnDamage(secondFighter));
      firstFighter.details.health = firstFighter.details.health - damage;

      if (damage === 0) {
        alert(secondFighter.details.name + ' missed.. Aww');
      } else {
        alert(firstFighter.details.name + ' hitted: -' + damage + '. Current health of ' + firstFighter.details.name + ': ' + firstFighter.details.health);
      }

      if (firstFighter.details.health <= 0) {
        clearInterval(interval);
        alert('Fight end. ' + 'Winner: ' + secondFighter.details.name);
      }

      turn = 0;
    }
  }, 500);
}

export default FightersView;
